/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  5                                     |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "system";
    object      fvSchemes;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

ddtSchemes
{
    default         backward;
}

gradSchemes
{
    default         Gauss linear;
}

divSchemes
{
    default                         none;
    "div\(phi,alpha.*\)"            Gauss vanLeer;
    "div\(phir,alpha.*,alpha.*\)"   Gauss vanLeer;
    "div\(alphaPhi.*,U.*\)"         Gauss limitedLinearV 1;
    "div\(alphaPhi.*,S.*\)"         Gauss limitedLinear 1;
    "div\(alphaPhi.*,X.*\)"         Gauss limitedLinear 1;
    div(Rc)                         Gauss linear;
    "div\(phi.*,U.*\)"              Gauss limitedLinearV 1;
    div(phi,k)                      bounded Gauss upwind;
    div(phi,omega)                  bounded Gauss upwind;
    div(Ji,Ii_h)                    bounded Gauss linearUpwind grad(Ii_h);
}

laplacianSchemes
{
    default                         Gauss linear corrected;
    "laplacian\(rAUf,.*\)"          Gauss linear corrected;
    laplacian(DkEff,k)              Gauss linear corrected;
    laplacian(DomegaEff,omega)      Gauss linear corrected;
    "laplacian\(\(alpha.*,U.*\)"    Gauss linear corrected;
    "laplacian\(.*,(S|X).*\)"       Gauss linear corrected;
}

interpolationSchemes
{
    default         linear;
}

snGradSchemes
{
    default         corrected;
}

wallDist
{
    method          meshWave; 
}

// ************************************************************************* //
